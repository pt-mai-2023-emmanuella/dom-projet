var container = document.querySelectorAll('.container');
var image = document.querySelectorAll('img');
var less =document.querySelectorAll('#less')
var plus = document.querySelectorAll('#plus')
var quantity = document.querySelectorAll('#quantity');
var total = document.querySelectorAll('#total');
var unit = document.querySelectorAll('#unit');
var main = document.querySelector('.main' )
var likes = document.querySelector('span.like')

for (const key in likes) {
    if (likes[key] && likes[key].style) {
        //likes[key].style.color = 'white';
        likes[key].style.cursor = "pointer";

        likes[key].addEventListener("click",(e)=>{
            let color = likes[key].style.color === 'white' ? 'red' : 'white';
            likes[key].style.color = color;
        })
    }
}


for (const key in container) {
    if (container[key] && container[key].style) {
        var containerdata=container[key]
        var imagedata=image[key]
        var lessdata=less[key]
        var plusdata=plus[key]
        var quantitydata=quantity[key]
       
        
        containerdata.style.display="flex"
        containerdata.style.justifyContent= "space-around"
        containerdata.style.alignItems= "center"
        containerdata.style.backgroundColor="pink"

        imagedata.style.width="150px"
        quantitydata.style.textAlign="center"
        quantitydata.style.width="50px"

        lessdata.style.backgroundColor="blue"
        lessdata.style.border="none"
        lessdata.style.padding="10px 20px"
        lessdata.style.color="white"
        lessdata.style.borderRadius="10px"
        lessdata.style.fontSize="25px"

        plusdata.style.backgroundColor="yellow"
        plusdata.style.border="none"
        plusdata.style.padding="10px 20px"
        plusdata.style.color="black"
        plusdata.style.borderRadius="10px"
        plusdata.style.fontSize="25px"
   }
}

for (const key in plus) {

    if (plus && plus[key].addEventListener) {
       
     plus[key].addEventListener("click",(e)=>{
        if (quantity[key]) {
            var value = quantity[key].value;
            if (value >= 0) {
            var qt =quantity[key].value = parseInt(value)+1
            total[key].textContent =qt*parseInt(unit[key].textContent);
            }
        } 
    }) 
    }      
}
for (const key in less) {
   if (less && less[key].addEventListener) {
    less[key].addEventListener("click",(e)=>{
        if (quantity[key]){
            var value = quantity[key].value;
            if (value > 1){
            var qt = quantity[key].value = parseInt(value) -1
            total[key].textContent = qt * parseInt(unit[key].textContent);
            }
        } 
    }) 
  } 
   
      
}
